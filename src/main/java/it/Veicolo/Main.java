package it.Veicolo;

public class Main {
    public static void main(String[] args) {
        VeicoloBase veicolo = new VeicoloBase("auto", 100, 0, 4, 67000);

        Accessorio gommeBelle = new Accessorio("Gomme belle", 3000);
        Accessorio specchietti = new Accessorio("specchietti", 500);
        Accessorio[] accessori = new Accessorio[veicolo.numAccessori];
        veicolo.setAccessori(accessori);
        veicolo.aggiungiAccessorio(gommeBelle);
        veicolo.stampaAccessori();
        System.out.println(veicolo.calcolaCostoTotale());
        veicolo.aggiungiAccessorio(specchietti);
        System.out.println(veicolo.calcolaCostoTotale());
    }
}
