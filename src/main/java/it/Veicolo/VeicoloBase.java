package it.Veicolo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VeicoloBase {

    protected String tipoVeicolo;
    protected int potenzaMotore;
    protected Accessorio[] accessori;
    protected int maxAccessori;
    protected int numAccessori;
    protected double prezzo;

    public VeicoloBase(String tipoVeicolo, int potenzaMotore, Accessorio[] accessori, int numAccessori, int maxAccessori, double prezzo) {
        this.tipoVeicolo = tipoVeicolo;
        this.potenzaMotore = potenzaMotore;
        accessori = new Accessorio[this.numAccessori];
        this.accessori = accessori;
        this.maxAccessori = maxAccessori;
        this.numAccessori = numAccessori;
        this.prezzo = prezzo;
    }

    public VeicoloBase(String tipoVeicolo, int potenzaMotore, int numAccessori, int maxAccessori, double prezzo) {
        this.tipoVeicolo = tipoVeicolo;
        this.potenzaMotore = potenzaMotore;
        this.maxAccessori = maxAccessori;
        this.numAccessori = numAccessori;
        this.prezzo = prezzo;
    }

    public VeicoloBase() {
    }

    public String getTipoVeicolo() {
        return tipoVeicolo;
    }

    public void setTipoVeicolo(String tipoVeicolo) {
        this.tipoVeicolo = tipoVeicolo;
    }

    public int getPotenzaMotore() {
        return potenzaMotore;
    }

    public void setPotenzaMotore(int potenzaMotore) {
        this.potenzaMotore = potenzaMotore;
    }

    public Accessorio[] getAccessori() {
        return accessori;
    }

    public void setAccessori(Accessorio[] accessori) {
        this.accessori = accessori;

    }

    public int getNumAccessori() {
        return numAccessori;
    }

    public void setNumAccessori(int numAccessori) {
        this.numAccessori = numAccessori;
    }

    public int getMaxAccessori() {
        return maxAccessori;
    }

    public void setMaxAccessori(int maxAccessori) {
        this.maxAccessori = maxAccessori;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    /**
     * metodo per aggiungere un accessorio al vettore di accessori, possibile solo se il numero di accessori presenti
     * è inferiore al limite
     * @param accessorio accessorio da aggiungere
     */
    public void aggiungiAccessorio(Accessorio accessorio) {
        if (numAccessori < maxAccessori) {
            numAccessori++;
            List accessoriList = new ArrayList(Arrays.asList(accessori));
            accessoriList.add(accessorio);
            accessori = (Accessorio[]) accessoriList.toArray(accessori);
            System.out.println("l'accessorio " + accessorio.getNome() +" è stato aggiunto correttamente");
        }
        else System.out.println("numero massimo di accessori raggiunto");
    }

    /**
     * metodo per calcolare la velocità massima in base alla potenza alla potenza del veicolo
     * @return velocità massima
     */
    public double calcolaVelocitaMassima(){
        return this.potenzaMotore*1.6;
    }

    /**
     * metodo per visualizzare gli accessori presenti
     */
    public void stampaAccessori(){
        for (int i = 0; i < numAccessori; i++){
            System.out.println(this.accessori[i].getNome());
        }
    }

    /**
     * metodo per calcolare il costo totale di veicolo + accessori
     * @return costo totale
     */
    public double calcolaCostoTotale(){
        double costoAccessori = 0;
        for (int i = 0; i < numAccessori; i++){
        costoAccessori += this.accessori[i].getPrezzo();
        }
        return costoAccessori + this.prezzo;
    }

    /**
     * metodo per calcolare il consumo totale di un viaggio dati la distanza e il consumo medio espresso in l/100km
     * @param distanza
     * @param consumoMedio
     * @return consumo totale stimato in litri
     */
    public double simulaViaggio(double distanza, double consumoMedio){
        return consumoMedio/100 * distanza;
    }




}
