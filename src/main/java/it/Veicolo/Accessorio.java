package it.Veicolo;

public class Accessorio {
    protected String nome;
    protected double prezzo;

    public Accessorio(String nome, double prezzo) {
        this.nome = nome;
        this.prezzo = prezzo;
    }
    public Accessorio() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
}
