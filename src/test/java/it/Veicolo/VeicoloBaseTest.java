package it.Veicolo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VeicoloBaseTest {

    static VeicoloBase veicolo;

    @BeforeEach
    void setUp(){
        veicolo = new VeicoloBase("auto", 100,0, 4, 67000 );;
    }


    @Test
    void testCalcolaVelocitaMassima() {
        assertEquals(160, veicolo.calcolaVelocitaMassima());
    }

    @Test
    void calcolaCostoTotale() {

        assertEquals(67000, veicolo.calcolaCostoTotale());

    }
    @Test
    void calcolaCostoTotaleConAccessori() {

        Accessorio gommeBelle = new Accessorio("Gomme belle", 3000);
        Accessorio specchietti = new Accessorio("specchietti", 500);
        Accessorio[] accessori = new Accessorio[veicolo.numAccessori];
        veicolo.setAccessori(accessori);
        veicolo.aggiungiAccessorio(gommeBelle);
        assertEquals(70000, veicolo.calcolaCostoTotale());
        veicolo.aggiungiAccessorio(specchietti);
        assertEquals(70500, veicolo.calcolaCostoTotale());
    }
}